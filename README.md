# SD remote code test

## Build dependencies
 - Node
 - Docker

# Background
Form manipulation and API queries are an essential part of the Datch Product. 
This test is designed to mimic a real world problem that we have faced at Datch in the past.

# Instructions
This code test involves setting up the git repository followed by two development tasks. The tasks are independent from each other, so feel free to complete them in any order. In the case that you are unfamiliar with the specific languages and frameworks of this project, please don't hesitate to simply describe the approach to solving each task in plain english. We're just as interested to see demonstrated general problem solving ability as to see the specific language/framework skills.

## Setup
At Datch we host our code on Gitlab(gitlab.com). To get started, please create a new Gitlab repository under your own account, configure it as the remote origin for this repo and push all branches. Add @benpurcell and @aricthorn as Maintainers to the project so we can review your submission.

## Task 1 - Performance 
For this task, we will be optimising some performance issues for the client, so please change your working directory to the client sub-folder.

Feel free to spin up the angular client and the mock backend to familiarize yourself with the base projects. You will find commands for starting the backend and app as npm scripts within the client directory.

You will notice that every time a user creates an "Issue Log" form, there is a considerable loading time for the form content. Our customers have complained about this and have asked if this performance issue can be rectified. We would like you to modify only the client to satisfy the customers request. Once you are satisfied with your update, generate a merge request back to the development branch and check the CI.

## Task 2 - Security
Currently the backend nestjs api only supports unencrypted http traffic. For this task, we want you to configure the backend service to serve the api over https on port 443, in addition to the existing http server on port 3000. You can use the provided self-signed certificate and key found in the backend/tls directory.

Please also adapt the backend docker files so that the API can be spun up using docker-compose and accessed using both https and http:
- https://localhost:443/api
- http://localhost:3000/api

Once you are satisfied with your update, generate a merge request back to the development branch and check the CI.