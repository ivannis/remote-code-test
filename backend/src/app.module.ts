import { Module } from '@nestjs/common';
import { EntityTypesModule } from './entity-types/entity-types.module';

@Module({
  imports: [ EntityTypesModule],
})
export class AppModule {}
