import { IsString, IsObject, IsOptional, IsArray, ValidateNested } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Entity, EntityType } from './entity-type.interface';
import { Type } from 'class-transformer';

export class EntityDto implements Entity {
  @ApiProperty({
    description: 'The name of the Entity',
    required: true,
    type: String,
    example: 'Brakes',
  })
  @IsString()
  value: string;

  @ApiProperty({
    description: 'Additional metadata for the Entity',
    required: false,
    type: Object,
    example: {
      manufacturer: 'Siemens',
      dateOfManufacture: '2016-05-08T00:00:00Z',
    },
  })
  @IsObject()
  @IsOptional()
  metaData?: object;
}

export class CreateEntityTypeDto implements EntityType {
  @ApiProperty({
    description: 'Additional metadata for the Component',
    required: false,
    isArray: true,
    type: EntityDto,
    example: [
      {
        value: 'Brakes',
        metaData: {
          manufacturer: 'Siemens',
          dateOfManufacture: '2016-05-08T00:00:00Z',
        },
      },
      {
        value: 'Shocks',
      },
    ],
  })
  @IsOptional()
  @Type(() => EntityDto)
  @ValidateNested()
  entities?: EntityDto[];
}

export class EntityTypeDto extends CreateEntityTypeDto {
  @ApiProperty({
    required: true,
    type: String,
  })
  @IsString()
  readonly _id: string;
}

export class ResponseEntityTypeDto {
  @ApiProperty({
    description: 'Response payload',
    required: false,
    type: EntityTypeDto,
  })
  payload: EntityTypeDto;

  @ApiProperty({
    description: 'Response message to display to user',
    required: false,
    type: String,
    example: 'EntityType message',
  })
  @IsString()
  msg: string;
}
export class ResponseGetEntitiesDto {
  @ApiProperty({
    description: 'Response payload',
    required: false,
    isArray: true,
    type: EntityDto,
    example: [
      {
        value: 'Brakes',
        metaData: {
          manufacturer: 'Siemens',
          dateOfManufacture: '2016-05-08T00:00:00Z',
        },
      },
      {
        value: 'Shocks',
      },
    ],
  })
  @Type(() => EntityDto)
  @ValidateNested()
  payload: EntityDto[];

  @ApiProperty({
    description: 'Response message to display to user',
    required: false,
    type: String,
    example: 'Found Entities message',
  })
  @IsString()
  msg: string;
}