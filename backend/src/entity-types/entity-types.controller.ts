import { Body, Controller, Get, Param, Post, HttpException, NotFoundException, BadRequestException, NotImplementedException, InternalServerErrorException } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { EntityTypesService as EntityTypesService } from './entity-types.service';
import { CreateEntityTypeDto, ResponseEntityTypeDto, ResponseGetEntitiesDto } from './interfaces/entity-type.dto';
import { Observable, throwError } from 'rxjs';
import { map, catchError, mergeMap, delay } from 'rxjs/operators';

// @ApiBearerAuth()
@ApiTags('entity-types')
@Controller('entity-types')
export class EntityTypesController {
  constructor(private readonly entityTypesService: EntityTypesService) { }

  @Post()
  @ApiOperation({ summary: 'Create entity type' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  create(@Body() createEntityTypeDto: CreateEntityTypeDto): Observable<ResponseEntityTypeDto> {
    return this.entityTypesService.create(createEntityTypeDto)
      .pipe(
        map((entityType) => {
          return {
            payload: entityType,
            msg: 'Entity type created',
          }
        }),
      );
  }

  @Get('/entities/:id')
  @ApiOperation({ summary: 'Get entities by entity type id' })
  @ApiResponse({
    status: 404,
    description: 'Entity type not found',
  })
  @ApiResponse({
    status: 501,
    description: 'Entities not found for specified entity type',
  })
  getEntities(@Param('id') id: string): Observable<ResponseGetEntitiesDto> {
    try {
      return this.entityTypesService.getEntities(id)
        .pipe(
          map((entities) => {
            return {
              payload: entities,
              msg: 'Found entities',
            }
          }),
          delay(2000),
          );
    } catch (err) {
      if (err.message === 'EntityTypeNotFound') {
        return throwError(new NotFoundException(`Entity type not found`));
      } else if (err.message === 'EntitiesArrayNotFoundForEntityType') {
        return throwError(new NotImplementedException(`Entities not found for specified entity type`));
      } else {
        console.error(err);
        return throwError(new InternalServerErrorException());
      }
    }
  }
}
