import { Test, TestingModule } from '@nestjs/testing';
import { EntityTypesService } from './entity-types.service';
import { CreateEntityTypeDto } from './interfaces/entity-type.dto';
import { doesNotReject } from 'assert';

describe('EntityTypesService', () => {
  let service: EntityTypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EntityTypesService],
    }).compile();

    service = module.get<EntityTypesService>(EntityTypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create an entity type', (done) => {
      const input: CreateEntityTypeDto = {
        entities: [
          {
            value: 'mockValue',
          },
        ],
      };
      service.create(input).subscribe(res => {
        expect(res).toEqual({
          '_id': jasmine.any(String),
          entities: [
            {
              value: 'mockValue',
            },
          ],
        });
        done();
      });
    });

    it('should ignore _id if passed in when creating entityType', (done) => {
      const mockService: any = service;
      mockService.entityTypes = [
        {
          _id: '0',
        },
      ];
      const input: CreateEntityTypeDto = {
        entities: [
          {
            value: 'mockValue',
          },
        ],
      };
      input['_id'] = '42';
      service.create(input).subscribe(res => {
        expect(res).toEqual({
          '_id': '1',
          entities: [
            {
              value: 'mockValue',
            },
          ],
        });
        done();
      });
    });
  });

  describe('getEntities', () => {
    it('should return array of entities for a given entity type id', (done) => {
      const mockService: any = service;
      mockService.entityTypes = [
        {
          _id: '0',
          entities: [
            {
              value: 'Brakes',
              metaData: {
                manufacturer: 'Siemens',
                dateOfManufacture: '2016-05-08T00:00:00Z',
              },
            },
            {
              value: "Shocks"
            },
          ],
        },
        {
          _id: '2',
          entities: [
            {
              value: 'Rakes',
              metaData: {
                manufacturer: 'Turnbull',
              },
            },
          ],
        },
      ];

      mockService.getEntities('0').subscribe(res => {
        expect(res).toEqual([
          {
            value: 'Brakes',
            metaData: {
              manufacturer: 'Siemens',
              dateOfManufacture: '2016-05-08T00:00:00Z',
            },
          },
          {
            value: "Shocks"
          },
        ]);
        done();
      });
    });

    it('should error when no entityType found for a given entityType id', (done) => {
      const mockService: any = service;
      // expect(mockService.getEntities('1')).toThrowError(Error('EntityTypeNotFound'));
      mockService.entityTypes = [
        {
          _id: '0',
          entities: [
            {
              value: 'Brakes',
              metaData: {
                manufacturer: 'Siemens',
                dateOfManufacture: '2016-05-08T00:00:00Z',
              },
            },
            {
              value: "Shocks"
            },
          ],
        },
        {
          _id: '2',
          entities: [
            {
              value: 'Rakes',
              metaData: {
                manufacturer: 'Turnbull',
              },
            },
          ],
        },
      ];
      try {
        mockService.getEntities('1').subscribe();
      } catch (err) {
        expect(err).toEqual(Error('EntityTypeNotFound'));
        done();
      }
    });

    it('should error when no entities array is found for the given entityType', (done) => {
      const mockService: any = service;
      // expect(mockService.getEntities('1')).toThrowError(Error('EntityTypeNotFound'));
      mockService.entityTypes = [
        {
          _id: '0',
        },
      ];
      try {
        mockService.getEntities('0').subscribe();
      } catch (err) {
        expect(err).toEqual(Error('EntitiesArrayNotFoundForEntityType'));
        done();
      }
    });
  });
});
