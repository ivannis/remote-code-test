import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { FormItemBase } from '../services/form-item-base';
import { FormControlService } from '../services/form-control.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [FormControlService]
})
export class DynamicFormComponent {

  formItems: FormItemBase<string>[];
  @Input('formItems') set setFormItems(formItems) {
    if (formItems) {
      this.formItems = formItems;
      this.form = this.fcs.toFormGroup(this.formItems);
    }
  }

  @Output() formSaved = new EventEmitter<any>();
  form: FormGroup;

  constructor(private fcs: FormControlService) { }

  onSubmit() {
    this.formSaved.emit(JSON.stringify(this.form.getRawValue()));
  }
}
