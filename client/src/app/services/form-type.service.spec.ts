import { TestBed } from '@angular/core/testing';
import { FormTypeService } from './form-type.service';


describe('FormTypeService', () => {
    let formTypeService: FormTypeService;


    beforeEach(() => {
        TestBed.configureTestingModule({ providers: [FormTypeService] });
        formTypeService = TestBed.inject(FormTypeService);
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    })

    it('getFormType with form name Issue Record should return the issue record form type', (done) => {
        formTypeService.getFormType('Issue Record')
            .subscribe(issueRecord => {
                expect(issueRecord).toEqual({
                    "formFields": [
                        { "controlType": "dropdown", "entityTypeId": "0", "key": "component", "label": "Component", "required": true },
                        { "controlType": "textbox", "key": "issue", "label": "Issue", "required": true }
                    ],
                    "id": "5e8ac57a09376e002c363f65",
                    "name": "Issue Record"
                });
                done();
            })
    });

    it('getFormType with a bad form name should return an error', (done) => {
        formTypeService.getFormType('Maintenance log')
            .subscribe(
                entities => { },
                error => {
                    expect(error).toEqual(`Maintenance log not implemented`);
                    done();
                }
            )
    });

});