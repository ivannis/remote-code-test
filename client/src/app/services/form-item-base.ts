export class FormItemBase<T> {
  value: T;
  key: string;
  label: string;
  required: boolean;
  order: number;
  controlType: string;
  entities: Array<any>;
  type: string;
  metaData: object;
  entityTypeId?: string;
  options: {key: string, value: string}[];

  constructor(options: {
      value?: T,
      key?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string,
      type?: string
      metaData?: object;
      entityTypeId?: string;
      entities?: Array<any>;
    } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.metaData = options.metaData || {};
    this.entityTypeId = options.entityTypeId || null;
    this.entities = options.entities || null;
  }
}
