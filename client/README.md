
### Installation

`npm install`

### Running

`npm run start`

### Running unit tests

`npm run test`

### Running e2e tests

Spin up backend - See backend instructions
Spin up client: `npm run start`
Run e2e tests: `npm run e2e`